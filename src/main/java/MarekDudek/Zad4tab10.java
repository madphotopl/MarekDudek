package MarekDudek;

import java.util.Scanner;

public class Zad4tab10 {
    public static void main(String[] args) {
        double sum = 0;
        double number;
        int a = 10;
        Scanner sc = new Scanner(System.in);
        for (int i=1; i<=a; i++){
            System.out.print("Podaj " + i + " liczbę: ");
            int wartosc = sc.nextInt();
            sum += wartosc;
            System.out.println();
        }
        number =  (sum/a);
        System.out.println(number);
    }

}
