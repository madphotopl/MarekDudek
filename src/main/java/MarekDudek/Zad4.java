package MarekDudek;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zad4 {
    private static final int NUMBER_OF_INTEGERS = 5;

    public static int validInt() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                int n = sc.nextInt();
                return n;

            } catch (InputMismatchException e) {
                sc.nextLine();
                System.out.println("wrong input");
            }
        }
    }

    public static void main(String[] args) {
        int sum = 0;
        int[] table = new int[NUMBER_OF_INTEGERS];
        for (int i = 0; i < NUMBER_OF_INTEGERS; i++) {

            System.out.printf("Enter %d. integer: \n", i + 1);
            table[i] = validInt();
            if (table[i] % 2 == 0) {
                sum += table[i];
            } else {
                sum -= table[i];
            }
        }
        System.out.println("sum of even and difference of odd numbers = "+sum);
    }

}

