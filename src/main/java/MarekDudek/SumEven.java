package MarekDudek;

import java.util.Scanner;

public class SumEven {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        for (int i = 1; i <= 5; i++) {
            System.out.print("Podaj " + i + " liczbę: ");
            int number = scanner.nextInt();
            if (number % 2 == 0) {
                sum = sum + number;
                //System.out.println("Podana liczba jest liczbą parzystą");
            } else {
                sum = sum - number;
                //System.out.println("Podana liczba jest liczbą nieparzystą");
            }
        }
        scanner.close();
        System.out.println("\n Wynik = " + sum);

    }
}
